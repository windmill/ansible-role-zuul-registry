==========================
ansible-role-zuul-registry
==========================

Ansible role to manage zuul-registry

* License: Apache License, Version 2.0
* Documentation: https://ansible-role-zuul-registry.readthedocs.org
* Source: https://opendev.org/windmill/ansible-role-zuul-registry
* Bugs: https://bugs.launchpad.net/ansible-role-zuul-registry

Description
-----------

This is a container image registry for use with the Zuul project gating system.

The defining feature of this registry is support for shadowing images: it
allows you to upload a local version of an image to use instead of an upstream
version. If you pull an image from this registry, it will provide the local
version if it exists, or the upstream if it does not.

Requirements
------------

* pip3 to be installed if using zuul_registry_install_method: (git|pip)

Packages
~~~~~~~~

Package repository index files should be up to date before using this role, we
do not manage them.

Role Variables
--------------

.. literalinclude:: ../../defaults/main.yaml
   :language: yaml
   :start-after: under the License.

Dependencies
------------

Example Playbook
----------------

.. code-block:: yaml

    - name: Install zuul-registry
      hosts: zuul-registry
      roles:
        - ansible-role-zuul-registry
